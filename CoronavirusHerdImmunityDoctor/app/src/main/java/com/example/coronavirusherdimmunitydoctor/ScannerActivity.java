package com.example.coronavirusherdimmunitydoctor;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coronavirusherdimmunitydoctor.models.CovidDialog;
import com.example.coronavirusherdimmunitydoctor.utils.Checksum;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * NOTE:
 * The Mobile Vision API supports the following formats of the barcode.
 *    - 1D barcodes: EAN-8, UPC-A, EAN-13, EAN-8, UPC-E, Code-93, Code-128, Code-39, Codabar, ITF.
 *    - 2D barcodes: QR Code, Data Matrix, AZTEC, PDF-417.
 */

public class ScannerActivity extends AppCompatActivity {

    private SurfaceView surfaceView;
    private BarcodeDetector codeDetector;
    private CameraSource camera;
    private String patient_id = "";  //code recognized

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_scanner_view);
        surfaceView = findViewById(R.id.surfaceView);
    }

    /**
     * Start camera and try to scan QR code
     */
    private void startScanner() {

        Toast.makeText(getApplicationContext(), R.string.toast_scanner_start, Toast.LENGTH_SHORT).show();
        codeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        camera = new CameraSource.Builder(this, codeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        //add camera to SurfaceView
        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {  //add camera to Surface
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    camera.start(surfaceView.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("ScannerActivity", "Exception camera start: " + e);
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                camera.stop();
            }
        });

        //manage code scanner
        codeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                //when scanner is released then it is stopped if patient_id is empty else QR code is recognized
                if (patient_id.isEmpty())
                {
                    Toast.makeText(getApplicationContext(), R.string.toast_scanner_stop, Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), R.string.toast_scanner_recognized, Toast.LENGTH_SHORT).show();
                    patient_id = "";
                }
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> code = detections.getDetectedItems();
                if (code.size() != 0) {
                    patient_id = code.valueAt(0).displayValue;  //get qrcode recognized

                    try {
                        if (Pattern.matches("covid-outbreak-control:\\d+",patient_id)) { //if QR code (patient ID) is right then compute checksum and go in ScannerSuccessActivity

                            long pat_id = Long.parseLong(patient_id.replaceAll("\\D+", ""));   //convert patient id in long
                            long checksum = Checksum.computeChecksum(pat_id);              //compute checksum by patient id in order to add it to patient id
                            final long pat_id_cs = Long.parseLong(Long.toString(pat_id) + Long.toString(checksum)); //add checksum as last digit of patient id

                            Intent intent = new Intent(getApplicationContext(), ScannerSuccessActivity.class);
                            intent.putExtra("patient id", pat_id_cs);   // give 'patientId+checksum' to next activity in order to display it
                            startActivity(intent);
                            finish();

                        } else{ //else if QR code is wrong
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    CovidDialog.warningDialog(ScannerActivity.this, getString(R.string.aldiag_wrong_scan_title) , getString(R.string.aldiag_wrong_scan));
                                }
                            });
                            Log.d("ScannerActivity", "Patient ID is wrong");
                        }

                    } catch (Exception e) { //when patient id is wrong (e.g.: incorrect regex, patient id is not convertable in long)
                        runOnUiThread(new Runnable() {
                            public void run() {
                                CovidDialog.warningDialog(ScannerActivity.this, getString(R.string.aldiag_wrong_scan_title) , getString(R.string.aldiag_wrong_scan));
                            }
                        });
                        Log.d("ScannerActivity", "Exception patient id convert: " + e);
                    }

                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        camera.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startScanner();
    }

}