package com.example.coronavirusherdimmunitydoctor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coronavirusherdimmunitydoctor.introduction.WelcomeActivity;
import com.example.coronavirusherdimmunitydoctor.models.CovidDialog;
import com.example.coronavirusherdimmunitydoctor.utils.PreferenceManager;
import com.example.coronavirusherdimmunitydoctor.utils.Security;


public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)

        this.setContentView(R.layout.activity_splashscreen);

        writeTitle();

        PreferenceManager pm = new PreferenceManager(this);
        if ( !pm.isFirstLogin()){  // if it is not the first login of this doctor then require to unlock app and open app

            Security security = new Security(this);
            security.unlockScreen(); //if device is secured then unlock and open app

        } else{ // else if it is first login then launch welcome activity

            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreenActivity.this, WelcomeActivity.class));
                    finish();
                }
            },3000);
        }
    }


    @Override
    public void onBackPressed() { } // disable back button

    private void writeTitle() {
        String blue = getResources().getString(R.string.splash_title_first);
        String next = getResources().getString(R.string.splash_title_next);
        TextView t = (TextView) findViewById(R.id.splashscreen_title);
        t.setText(Html.fromHtml("<font color='#16ACEA'>"+blue+"</font>" + next));
    }

    /**
     * Manage response of startActivityForResult called in unlockScreen:
     *    - If requestCode is equal to "LOCK_REQUEST_CODE" -> is the response to unlockScreen
     *    - If requestCode is equal to "SECURITY_SETTING_REQUEST_CODE" -> is the response to createLockScreen
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final int LOCK_REQUEST_CODE = 221;
        final int SECURITY_SETTING_REQUEST_CODE = 233;

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCK_REQUEST_CODE:             //response to unlockScreen
                if (resultCode == Activity.RESULT_OK) { //if PIN is inserted and right
                    startActivity(new Intent(this, LoginActivity.class));
                    finish();
                } else{ // else if PIN is not inserted
                    Toast.makeText(this,R.string.pin_canceled,Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
            case SECURITY_SETTING_REQUEST_CODE:  //response to createLockScreen

                //When user is enabled Security settings then we don't get any kind of RESULT_OK
                //So we need to check whether device has enabled screen lock or not
                Security security = new Security(this);
                if (security.isDeviceSecured()) {  //If screen lock is created and enabled then start intent to unlock screen

                    CovidDialog.warningDialog(SplashScreenActivity.this, getString(R.string.security_title), getString(R.string.device_is_secure));
                    security.unlockScreen();

                } else {//If screen lock is not enabled just update text

                    CovidDialog.warningDialog(SplashScreenActivity.this, getString(R.string.security_title), getString(R.string.security_device_cancelled));
                }
                break;
        }
    }

}