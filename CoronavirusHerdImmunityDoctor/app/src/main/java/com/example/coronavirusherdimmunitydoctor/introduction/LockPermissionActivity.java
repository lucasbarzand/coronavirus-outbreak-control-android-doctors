package com.example.coronavirusherdimmunitydoctor.introduction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coronavirusherdimmunitydoctor.LoginActivity;
import com.example.coronavirusherdimmunitydoctor.R;
import com.example.coronavirusherdimmunitydoctor.models.CovidDialog;
import com.example.coronavirusherdimmunitydoctor.models.TouchListener;
import com.example.coronavirusherdimmunitydoctor.utils.Security;


public class LockPermissionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        setContentView(R.layout.intro3_lockpermission);

        Button button_next;


        button_next = findViewById(R.id.button_next);
        TouchListener.buttonClickEffect(button_next);   //add click button effect
        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Security security = new Security(LockPermissionActivity.this);
                security.unlockScreen();  //unlock app or create new lock
            }
        });

    }

    /**
     * Manage response of startActivityForResult called in unlockScreen:
     *    - If requestCode is equal to "LOCK_REQUEST_CODE" -> is the response to unlockScreen
     *    - If requestCode is equal to "SECURITY_SETTING_REQUEST_CODE" -> is the response to createLockScreen
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final int LOCK_REQUEST_CODE = 221;
        final int SECURITY_SETTING_REQUEST_CODE = 233;

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCK_REQUEST_CODE:             //response to unlockScreen
                if (resultCode == Activity.RESULT_OK) { //if PIN is inserted and right
                    startActivity(new Intent(this, LoginActivity.class));
                    finish();
                } else { // else if PIN is not inserted
                    Toast.makeText(this, R.string.pin_canceled, Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
            case SECURITY_SETTING_REQUEST_CODE:  //response to createLockScreen

                //When user is enabled Security settings then we don't get any kind of RESULT_OK
                //So we need to check whether device has enabled screen lock or not
                Security security = new Security(this);
                if (security.isDeviceSecured()) {  //If screen lock is created and enabled then start intent to unlock screen

                    CovidDialog.warningDialog(LockPermissionActivity.this, getString(R.string.security_title), getString(R.string.device_is_secure));
                    security.unlockScreen();

                } else {//If screen lock is not enabled just update text

                    CovidDialog.warningDialog(LockPermissionActivity.this, getString(R.string.security_title), getString(R.string.security_device_cancelled));
                }
                break;
        }
    }

    @Override
    public void onBackPressed() { } // disable back button


}
