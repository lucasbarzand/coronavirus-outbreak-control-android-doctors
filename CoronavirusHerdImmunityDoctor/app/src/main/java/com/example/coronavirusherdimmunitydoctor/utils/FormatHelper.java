package com.example.coronavirusherdimmunitydoctor.utils;

import android.util.Log;

import com.example.coronavirusherdimmunitydoctor.models.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class FormatHelper {

    private static FormatHelper instance = new FormatHelper();

    public static FormatHelper getInstance() {
        return instance;
    }

    public static String BR_cellphone_pattern = "^\\+[5]{2}[1-9]{2}9[1-9]{4}[0-9]{4}$";
    /** +[5]{2} - internacional code, start with + followed by 55
     * [1-9]{2} - area code (X)
     * 9[1-9]{4} - must start with 9 followed by 4 digits between 1-9 (Y)
     * [0-9]{4} - 4 digits between 0-9 (D)
     * +55XX9YYYYDDDD
     */
    private static HashMap<String,String> pattern_by_ISO= new HashMap<String,String>();

    static {
        pattern_by_ISO.put("BR", BR_cellphone_pattern);
    }


    private FormatHelper() {

    }

    public HashMap<Contact, ArrayList<String>> sort(HashMap<Contact,ArrayList<String>> unsortedMap){
        HashMap<Contact,ArrayList<String>> sorted = null;

        return sorted;
    }

    public boolean isCellPhoneCorrectFormat(String given_cellphone, String ISO){
        boolean result = false;
        Log.i("Cellphone.Validate: ",given_cellphone);
        String relativePattern = pattern_by_ISO.get(ISO);
        if(relativePattern!=null)
            result = Pattern.matches(BR_cellphone_pattern, given_cellphone);
        else
            result = true; //Returns true and does not apply the pattern check because it does not have the regex for that ISO yet.

        return result;
    }

    public String correctPhoneNumber(String invited, String doctor_cellphone, String ISO){
        String corrected = "";

        if(ISO.equals("BR")) //we have phone number correction function only for Brazil
            corrected = correctPhoneNumberToBR(invited,doctor_cellphone);

        return corrected;
    }

    public String correctPhoneNumberToBR(String invited, String doctor_cellphone){
        int expected_size = doctor_cellphone.length();

        String country_code = doctor_cellphone.substring(0,3); //these index depend on the standard of the telephone number of each country
        String area_code = doctor_cellphone.substring(3,5);
        String invited_corrected="",corrected="";

        if(invited.length()<expected_size) {
            String rest = invited.substring(3, invited.length());
            switch(rest.length()) {
                    case 10: //missing extra '9'
                        String lst_part_number = rest.substring(2);
                        if(lst_part_number.startsWith("9") || lst_part_number.startsWith("8")) //'cause every cellphone in Brasil starts with 8 ou 9
                            corrected = rest.substring(0,2)+"9"+lst_part_number;
                        break;
                    case 9: //with extra '9' but missing area_code
                        if(rest.startsWith("9")|| rest.startsWith("8"))
                            corrected = area_code+rest;
                        break;
                    case 8: //missing either extra '9' and area_code
                        if(rest.startsWith("9")|| rest.startsWith("8"))
                            corrected = area_code+"9"+rest;
                        break;
                    default:
                        System.out.println("unexpected: "+rest.length());
                        break;
                }
                if(corrected.length()>0)
                    invited_corrected = country_code+corrected;
        }
        return invited_corrected;
    }


}
