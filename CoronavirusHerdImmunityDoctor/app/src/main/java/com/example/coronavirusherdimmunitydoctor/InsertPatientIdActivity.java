package com.example.coronavirusherdimmunitydoctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.coronavirusherdimmunitydoctor.models.CovidDialog;
import com.example.coronavirusherdimmunitydoctor.models.TouchListener;
import com.example.coronavirusherdimmunitydoctor.utils.ApiManager;
import com.example.coronavirusherdimmunitydoctor.utils.Checksum;
import com.example.coronavirusherdimmunitydoctor.utils.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import okhttp3.Response;

public class InsertPatientIdActivity extends AppCompatActivity {

    private AutoCompleteTextView ACTV_patientID;
    private Button bt_check_id;
    private FloatingActionButton bt_back;
    private ArrayList<Long> suspected_patients_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_patient_id);

        /***************** set AutoCompleteTextView "ACTV_patientID"****************/

        ACTV_patientID = findViewById(R.id.ACTV_patientID);
        suspected_patients_list = new ArrayList<Long>();
        PreferenceManager pm = new PreferenceManager(getApplicationContext());
        task_getSuspectListbyDocId(pm.getDoctorId());     // get list of suspected Patients
        ArrayAdapter<Long> patients_list_adapter = new ArrayAdapter<Long>
                (this, android.R.layout.select_dialog_item, suspected_patients_list);
        ACTV_patientID.setAdapter(patients_list_adapter); //put list of suspected Patients on AutoCompleteTextView
        ACTV_patientID.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event){
                ACTV_patientID.showDropDown();  //When touch AutoCompleteTextView then show list of patients id
                return false;
            }
        });
        ACTV_patientID.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) ||
                                      (actionId == EditorInfo.IME_ACTION_DONE)        ||
                                      (actionId == EditorInfo.IME_ACTION_NEXT) ) {

                    //if you click "enter", "done" or "next" on keyboard, keyboard drops down
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                }
                return false;
            }
        });

        /***************** set button "bt_check_id" ***********************/

        bt_check_id = findViewById(R.id.bt_check_id);
        TouchListener.buttonClickEffect(bt_check_id);   //add click button effect
        bt_check_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ACTV_patientID.getText().length() == 0){
                    Toast.makeText(InsertPatientIdActivity.this, R.string.toast_err_empty_id, Toast.LENGTH_SHORT).show();

                } else if (Checksum.verifyChecksum(Long.parseLong(ACTV_patientID.getText().toString()))){ //if checksum is verified then go to change status
                    try {
                            long pat_id_cs = Long.parseLong(ACTV_patientID.getText().toString()); //patient id + checksum
                            Intent intent=new Intent(InsertPatientIdActivity.this, ChangeStatusActivity.class);
                            intent.putExtra("patient id", pat_id_cs);    // give 'patientId + checksum' to next activity
                            startActivity(intent);
                            finish();

                    } catch (NumberFormatException e) { //when patient id is not convertable in a long
                        CovidDialog.warningDialog(InsertPatientIdActivity.this, getString(R.string.aldiag_wrong_scan_title) , getString(R.string.aldiag_wrong_scan));
                    }
                } else {
                    new AlertDialog.Builder(InsertPatientIdActivity.this)
                            .setTitle("ERROR")
                            .setMessage(R.string.alert_err_check_patid)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", null)
                            .show();
                }
            }
        });

        /***************** set button "bt_back" ***********************/

        bt_back = findViewById(R.id.bt_back);
        bt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            /** Click on "back" button */
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    /******************* Task Function **************************/

    /**
     * Run task in order to call task_getSuspectListbyDocId API and manage the response
     *
     * @param doctor_id: doctor have scanned list of suspected patients
     */
    private void task_getSuspectListbyDocId(final long doctor_id) {

        Task.callInBackground(new Callable<JSONArray>() {
            @Override
            public JSONArray call() throws Exception {

                PreferenceManager pm = new PreferenceManager(getApplicationContext());
                Boolean updated = false;
                JSONArray patients_list = null;

                while (updated == false) {

                    Response response_suspectedListbyDocId = ApiManager.getSuspectListbyDocId(doctor_id, pm.getJwtToken());  //call getSuspectListbyDocId

                    if (response_suspectedListbyDocId != null) {
                        switch (response_suspectedListbyDocId.code()) { //check response status(code)
                            case 200:     // if response is 'ok' -> list of Suspected Patients has been received
                                try {
                                    Log.d("task_getSuspectList", "list of Suspected has been received");
                                    updated = true;

                                    String strResponse_body = response_suspectedListbyDocId.body().string();      //get body of Response
                                    JSONObject response_body = new JSONObject(strResponse_body);
                                    patients_list = response_body.getJSONArray("data");           //get list of suspected patients

                                } catch (Exception e)
                                {
                                    Log.d("task_getSuspectList", "Exception to read body received");
                                }
                                break;

                            case 403:    // if jwt token is expired/not found/invalid -> call refreshJwtToken and recall getSuspectListbyDocId
                                Log.d("task_getSuspectList", "Jwt Token expired");

                                Response response_refreshJwtToken = ApiManager.refreshJwtToken(pm.getAuthorizationToken());  //call refreshJwtToken

                                if (response_refreshJwtToken != null &&
                                        response_refreshJwtToken.code() == 200) { //check response status(code)

                                    try {
                                        String strResponse_body = response_refreshJwtToken.body().string(); //get body of Response
                                        JSONObject response_body = new JSONObject(strResponse_body);
                                        pm.setJwtToken(response_body.getString("token"));             //save new Jwt Token in shared preferences

                                    } catch (Exception e) {
                                        Log.d("task_getSuspectList", "Exception to read jwt token received");
                                    }

                                }
                                break;

                            case 429: //rate limiter reached up in 1 minute, doctor has to wait to call this api
                                Log.d("task_getSuspectList", "rate limiter reached up in 1 minute");
                                updated = true;
                                break;

                            default:
                                Log.d("task_getSuspectList", "Code not recognized:" + response_suspectedListbyDocId.code());
                                updated = true;
                                break;
                        }
                    } else { // no response from Backend (like: internet disabled)
                        Log.d("task_getSuspectList", "No response by task_getSuspectListbyDocId");
                        updated = true;
                    }
                }
                return patients_list;

            }
        }).onSuccess(new Continuation<JSONArray, Object>() {
            @Override
            public Object then(Task<JSONArray> task) throws Exception {

                JSONArray patients_list = task.getResult();
                if(patients_list != null){
                    //convert JSONArray in String Array in order to add "patient_id"
                    for (int i=0;i<patients_list.length();i++){
                        
                        long pat_id = patients_list.getJSONObject(i).getLong("patient_id"); //get suspected patient id
                        long checksum = Checksum.computeChecksum(pat_id);                         //compute checksum
                        long pat_id_cs = Long.parseLong(Long.toString(pat_id) + Long.toString(checksum)); //add checksum as last digit of patient id
                        suspected_patients_list.add(pat_id_cs);
                    }
                    Collections.sort(suspected_patients_list);  //sort list in ascending order
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

}
