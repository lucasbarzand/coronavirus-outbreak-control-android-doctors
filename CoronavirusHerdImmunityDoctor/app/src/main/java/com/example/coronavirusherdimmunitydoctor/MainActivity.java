package com.example.coronavirusherdimmunitydoctor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.coronavirusherdimmunitydoctor.invitecontacts.InviteContactsActivity;
import com.example.coronavirusherdimmunitydoctor.models.TouchListener;
import com.example.coronavirusherdimmunitydoctor.utils.PermissionRequest;


import java.util.ArrayList;


public class MainActivity extends Activity {

    private FrameLayout bt_scan_qrcode;
    private FrameLayout bt_change_status;
    private Button bt_invite_doc;
    private Button bt_howitworks;

    private ArrayList<String> contacts_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_doctor_view);

        bt_scan_qrcode = findViewById(R.id.bt_scan_qrcode);
        TouchListener.buttonClickEffect(bt_scan_qrcode);   //add click button effect
        bt_scan_qrcode.setOnClickListener(new View.OnClickListener() {

            @Override
            /**
             * Click on "Scan QR code" button, open camera and try to recognize QR Code
             */
            public void onClick(View view)
            {
                Intent intent=new Intent(MainActivity.this, ScannerActivity.class);
                startActivity(intent);
            }
        });

        bt_change_status = findViewById(R.id.bt_change_status);
        TouchListener.buttonClickEffect(bt_change_status);  //add click button effect
        bt_change_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, InsertPatientIdActivity.class);
                startActivity(intent);
            }
        });

        bt_invite_doc = findViewById(R.id.bt_invite_doc);
        TouchListener.buttonClickEffect(bt_invite_doc);     //add click button effect
        bt_invite_doc.setOnClickListener(new View.OnClickListener() {

            @Override
            /**
             * Click on "Invite new Doctors" button, go to another activity in order to select Contacts
             * and invite them to Server
             **/
            public void onClick(View view)
            {
                Intent intent=new Intent(MainActivity.this, InviteContactsActivity.class);
                startActivity(intent);
            }
        });

        bt_howitworks = findViewById(R.id.bt_howitworks);
        TouchListener.buttonClickEffect(bt_howitworks);     //add click button effect
        bt_howitworks.setOnClickListener(new View.OnClickListener() {

            @Override
            /**
             * Click on "Invite new Doctors" button, go to another activity in order to select Contacts
             * and invite them to Server
             **/
            public void onClick(View view)
            {
                Intent intent=new Intent(MainActivity.this, HowItWorksActivity.class);
                startActivity(intent);
            }
        });

        PermissionRequest permissions = new PermissionRequest(MainActivity.this);
        permissions.checkPermissions(); //check if camera and read_contacts are enabled else go to activity in order to enable them
    }


    /* Manage back button when is pressed in order to exit from application*/
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // builder.setCancelable(false);
        builder.setTitle(R.string.alert_exit_title);
        builder.setMessage(R.string.alert_exit_msg);
        builder.setPositiveButton(R.string.alert_exit_pos_bt, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();               //close activity
                System.exit(0);  //close app
            }
        });
        builder.setNegativeButton(R.string.alert_exit_neg_bt, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert=builder.create();
        alert.show();
    }
}