package com.example.coronavirusherdimmunitydoctor.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coronavirusherdimmunitydoctor.LoginActivity;
import com.example.coronavirusherdimmunitydoctor.R;
import com.example.coronavirusherdimmunitydoctor.models.CovidDialog;


public class Security extends Activity {

    private Activity _activity;

    private static final int LOCK_REQUEST_CODE = 221;
    private static final int SECURITY_SETTING_REQUEST_CODE = 233;

    public Security(Activity activity){
        _activity = activity;
    }

    /**
     * if device is secured then unlock Screen else create a PIN, pattern or fingerprint
     */
    public void unlockScreen(){

        if (isDeviceSecured()) {     // Check is device is secured by a PIN, pattern or password, in this case try to unlock screen

            final AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
            builder.setTitle(_activity.getString(R.string.security_title));
            builder.setMessage(_activity.getString(R.string.security_unlock_screen));
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    KeyguardManager keyguardManager = (KeyguardManager) _activity.getSystemService(Context.KEYGUARD_SERVICE);
                    Intent i = keyguardManager.createConfirmDeviceCredentialIntent(
                            _activity.getResources().getString(R.string.unlock),
                            _activity.getResources().getString(R.string.confirm_pattern));

                    if ( i != null){
                        try {
                            //Start activity for result
                            _activity.startActivityForResult(i, LOCK_REQUEST_CODE);

                        }catch (Exception e) {

                            //If some exception occurs means Screen lock is not set up please set screen lock
                            Log.d("unlockScreenDialog", "Exception, Screen lock is not set up: " + e);
                        }
                    }

                }
            });
            builder.setCancelable(false);
            builder.show();

        } else{  //if device is not secured then create lock screen

            createLockScreen();
        }
    }

    /**
     * Check is device is secured by a PIN, pattern or password
     * @return if sdk android api >= 23 -> return true if a PIN, pattern or password was set
     *         else -> return true if a PIN, pattern or password is set or a SIM card is locked
     */
    public boolean isDeviceSecured() {
        KeyguardManager keyguardManager = (KeyguardManager) _activity.getSystemService(Context.KEYGUARD_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { // if api >= 23

            return keyguardManager.isDeviceSecure(); //true if a PIN, pattern or password was set.
        }
        return keyguardManager.isKeyguardSecure (); //true if a PIN, pattern or password is set or a SIM card is locked.
    }

    /**
     * Allow to open setting in order to create a PIN or pattern or fingerprint to lock screen
     * Force lock screen to open App
     */
    private void createLockScreen(){

        Toast.makeText(_activity, _activity.getString(R.string.create_lock_screen), Toast.LENGTH_LONG).show();

        //Open Security screen directly to enable patter lock
        Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
        try {

            //Start activity for result
            _activity.startActivityForResult(intent, SECURITY_SETTING_REQUEST_CODE);
        } catch (Exception e) {

            Log.d("createLockScreen","Exception, app not find any Security:" + e);
            //If app is unable to find any Security settings then user has to set screen lock manually
            CovidDialog.warningDialog(_activity, _activity.getString(R.string.security_title), _activity.getString(R.string.setting_label));
        }

    }

}
