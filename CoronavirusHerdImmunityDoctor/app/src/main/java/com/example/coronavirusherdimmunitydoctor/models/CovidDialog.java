package com.example.coronavirusherdimmunitydoctor.models;

import android.app.AlertDialog;
import android.content.Context;


public class CovidDialog {

    /**
     * Create a dialog where notifies a "warning" message
     * @param context
     * @param title
     * @param msg
     */
    public static void warningDialog(Context context, String title, String msg){

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }

    /**
     * Create a dialog where notifies a "error" message (like internet disabled)
     * @param context
     * @param title
     * @param msg
     */
    public static void errorDialog(Context context, String title, String msg){

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }

    /**
     * Create a dialog where notifies to "unlock screen" in order to use APP
     * When you click on "ok" then you have to put password, PIN or fingerprint which is the same to unlock screen
     * @param context
     * @param title
     * @param msg
     */
    public static void unlockScreenDialog(final Context context, String title, String msg){

    }

    /**
     * Create a dialog where requires a "permission"
     * @param title
     * @param msg
     */
    public void permissionDialog(String title, String msg){

    }


    /**
     * Create a dialog where ask if you are sure to "validate" the action
     * @param title
     * @param msg
     */
    public void validationDialog(String title, String msg){

    }



}
