package com.example.coronavirusherdimmunitydoctor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.coronavirusherdimmunitydoctor.enums.PatientStatus;
import com.example.coronavirusherdimmunitydoctor.models.CovidDialog;
import com.example.coronavirusherdimmunitydoctor.models.TouchListener;
import com.example.coronavirusherdimmunitydoctor.utils.ApiManager;
import com.example.coronavirusherdimmunitydoctor.utils.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import okhttp3.Response;


public class ChangeStatusActivity extends Activity {

    private TextView tv_changestatus_patient_id;
    private Button bt_confirm_covid, bt_negative_covid, bt_recover_covid, bt_back;

    private boolean task_activated_updateUserStatus = true;               //used to call just one by one the task "task_updateUserStatus"

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_patient_status);

        Intent intent = getIntent();
        final long patient_id_with_checksum = intent.getLongExtra("patient id", -1); //get 'patient id + checksum'
        final long patient_id = patient_id_with_checksum / 10;                  //patient id without checksum (removing last digit)

        final boolean ignore_status_check = intent.getBooleanExtra("ignore status check", false);   // get 'ignore_status_check' if is true then old patient status (Suspected) is ignored

        /*** Set text "Patient ID: patientId + checksum" *****/
        tv_changestatus_patient_id = findViewById(R.id.tv_changestatus_patient_id);
        String tv_pat_id_cs = String.format(getString(R.string.tv_changestatus_patient_id), patient_id_with_checksum); //add 'patient id + checksum' on string
        tv_changestatus_patient_id.setText(tv_pat_id_cs); //Set text "Patient ID: patientId + checksum"

        /****************** Confirm Button *******************/

        bt_confirm_covid = findViewById(R.id.bt_confirm_covid);
        TouchListener.buttonClickEffect(bt_confirm_covid);   //add click button effect
        bt_confirm_covid.setOnClickListener(new View.OnClickListener() {

            @Override
            /**
             * Click on "Positive to Covid 19" button, change patient's status to 'infected'
             */
            public void onClick(View view)
            {

                if (task_activated_updateUserStatus) { //used to call just one by one the task "task_updateUserStatus"
                    task_activated_updateUserStatus = false; //become false so that task "task_updateUserStatus" cannot be called again

                    task_updateUserStatus(patient_id, "INFECTED", ignore_status_check);     //"infected"
                }

            }
        });

        /****************** Negative Button *******************/

        bt_negative_covid = findViewById(R.id.bt_negative_covid);
        TouchListener.buttonClickEffect(bt_negative_covid);   //add click button effect
        bt_negative_covid.setOnClickListener(new View.OnClickListener() {

            @Override
            /**
             * Click on "Negative to Covid-19" button, change patient's status to 'negative'
             */
            public void onClick(View view)
            {
                if (task_activated_updateUserStatus) { //used to call just one by one the task "task_updateUserStatus"
                    task_activated_updateUserStatus = false; //become false so that task "task_updateUserStatus" cannot be called again

                    task_updateUserStatus(patient_id, "NEGATIVE", ignore_status_check);     //"negative"
                }
            }
        });


        /****************** Recover Button *******************/

        bt_recover_covid = findViewById(R.id.bt_recover_covid);
        TouchListener.buttonClickEffect(bt_recover_covid);   //add click button effect
        bt_recover_covid.setOnClickListener(new View.OnClickListener() {

            @Override
            /**
             * Click on "Healed" button, change patient's status to 'healed'
             */
            public void onClick(View view)
            {
                if (task_activated_updateUserStatus) { //used to call just one by one the task "task_updateUserStatus"
                    task_activated_updateUserStatus = false; //become false so that task "task_updateUserStatus" cannot be called again

                    task_updateUserStatus(patient_id, "HEALED", ignore_status_check);        //"healed"

                }
            }
        });

        /****************** Back Button *******************/

        bt_back = findViewById(R.id.bt_back);
        bt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }



    /******************** Task Functions *********************************************/

    private static final HashMap<String, Integer> status_map= new HashMap<>();
    static {
        status_map.put("INFECTED", R.string.status_infected);
        status_map.put("NEGATIVE", R.string.status_negative);
        status_map.put("HEALED", R.string.status_healed);
    };

    /**
     * Run task in order to call updateUserStatus API and manage the response
     *
     * @param user_id: patient id
     * @param str_new_status: health patient status (string)
     * @param ignore_status_check: ignore status check (boolean)
     */
    private void task_updateUserStatus(final long user_id, final String str_new_status, final boolean ignore_status_check){

        Toast.makeText(getApplicationContext(), R.string.toast_click_status_changed, Toast.LENGTH_SHORT).show();

        Task.callInBackground(new Callable<String>() {
            @Override
            public String call() throws Exception {

                Integer new_status = PatientStatus.valueOf(str_new_status).ordinal();     // get 'enum' value of PatientStatus

                PreferenceManager pm = new PreferenceManager(getApplicationContext());
                Boolean updated =  false;
                String ret_value = "";

                while ( updated == false){

                    Response response_updateUS = ApiManager.updateUserStatus(user_id, new_status, pm.getJwtToken(), ignore_status_check);  //call updateUserStatus

                    if (response_updateUS != null) {
                        switch (response_updateUS.code()) {//check response status(code)
                            case 200:     // if response is 'ok' -> status has been changed
                                Log.d("task_updateUserStatus","status has been changed");
                                ret_value = "chg_st";
                                updated = true; //exit while
                                break;
                            case 403:     // if jwt token is not sent -> call refreshJwtToken and recall task_updateUserStatus
                            case 401:     // if jwt token is expired -> call refreshJwtToken and recall task_updateUserStatus
                                Log.d("task_updateUserStatus","Jwt Token expired");

                                Response response_refreshJwtToken = ApiManager.refreshJwtToken(pm.getAuthorizationToken());  //call refreshJwtToken
                                if (response_refreshJwtToken != null &&
                                        response_refreshJwtToken.code() == 200){ //check response status(code)

                                    try{
                                        String strResponse_body = response_refreshJwtToken.body().string();  //get body of Response
                                        JSONObject response_body = new JSONObject(strResponse_body);
                                        pm.setJwtToken(response_body.getString("token"));              //save new Jwt Token in shared preferences

                                    }catch (Exception e){
                                        Log.d("task_updateUserStatus", "Exception to read jwt token received: " + e);
                                    }
                                }
                                break;
                            case 400:      // patient id status was not 'Suspected' then it cannot be changed to 'Positive'
                                Log.d("task_updateUserStatus", "Status cannot be changed to 'Positive' because was not 'Suspected'");
                                ret_value = "not_pos";
                                updated = true; //exit while
                                break;
                            case 429: //rate limiter reached up in 1 minute, doctor has to wait to call this api
                                Log.d("task_updateUserStatus", "rate limiter reached up in 1 minute");
                                ret_value = "ratelim";
                                updated = true;
                                break;
                            default:       // for example, if patient id is not recognized
                                Log.d("task_updateUserStatus", "Code not recognized:"+response_updateUS.code());
                                ret_value = "not_rec";
                                updated = true; //exit while
                                break;
                        }
                    } else{  // no response from Backend (like: internet disabled)
                        Log.d("task_updateUserStatus","No response by updateUserStatus");
                        ret_value = "no_resp";
                        updated = true; //exit while
                    }
                }

                return ret_value;
            }
        }).onSuccess(new Continuation<String, Object>() {
            @Override
            public String then(Task<String> task) throws Exception {

                switch (task.getResult()) {
                    case "chg_st":  //health status changed
                        String msg = String.format(getString(R.string.aldiag_status_changed), getString(status_map.get(str_new_status))); //add 'new status' on string
                        CovidDialog.warningDialog(ChangeStatusActivity.this, getString(R.string.aldiag_title_change_status), msg); //display warning dialog
                        break;
                    case "not_rec": //patient id (QR code) not recognized
                        CovidDialog.warningDialog(ChangeStatusActivity.this, getString(R.string.aldiag_title_change_status), getString(R.string.aldiag_code_not_recognized)); //display warning dialog
                        break;
                    case "no_resp": // no response from Backend (like: internet disabled)
                        CovidDialog.warningDialog(ChangeStatusActivity.this, getString(R.string.aldiag_title_change_status), getString(R.string.aldiag_no_resp_change_status)); //display warning dialog
                        break;
                    case "not_pos": // patient id status was not 'Suspected' then it cannot be changed to 'Positive'
                        CovidDialog.warningDialog(ChangeStatusActivity.this, getString(R.string.aldiag_title_change_status), getString(R.string.aldiag_not_positive)); //display warning dialog
                        break;
                    case "ratelim": //rate limiter reached up in 1 minute, doctor has to wait to call this api
                        CovidDialog.errorDialog(ChangeStatusActivity.this,
                                getString(R.string.aldiag_err_updatestatus_title),
                                getString(R.string.aldiag_ratelimiter_updatestatus)); //display error dial
                    default: //some errors
                        CovidDialog.warningDialog(ChangeStatusActivity.this, getString(R.string.aldiag_title_change_status), getString(R.string.aldiag_err_status_change)); //display warning dialog
                        break;
                }

                task_activated_updateUserStatus = true;    //become true so that task "task_updateUserStatus" can be called again
                return null;
            }
        },  Task.UI_THREAD_EXECUTOR);
    }



}